const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const router = express.Router();
//mongoose.connect('mongodb://localhost/ReactBooks');
const connectionString = 'mongodb+srv://TobiasSecher:rJ.BejAatvzXS4y@cluster0-moh1p.mongodb.net/Reactoverflow?retryWrites=true';
// const connectionString = 'mongodb+srv://TobiasSecher:rJ.BejAatvzXS4y@cluster0-tovqd.mongodb.net/test?retryWrites=true';
mongoose.connect(connectionString, { useNewUrlParser: true }, (err) => { console.log('mongo db connection', err) });


const app = express();
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../build')));
/****** Configuration *****/
const port = (process.env.PORT || 8080);

// Additional headers to avoid triggering CORS security errors in the browser
// Read more: https://en.wikipedia.org/wiki/Cross-origin_resource_sharing
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");

    // intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
        // respond with 200
        console.log("Allowing OPTIONS");
        res.sendStatus(200);
    } else {
        // move on
        next();
    }
});
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("DB connection is open!");
});

/****** Data *****/

/****** Helper functions *****/
function getFromId(id) {
    return books.find((elm) => elm.id === Number(id));
}

function findNextId() {
    const reducer = (acc, curr) => Math.max(acc, curr);
    let nextId = books.map(el => el.id).reduce(reducer) + 1;
    return nextId;
}

let AnswerSchema = new mongoose.Schema({
    comment: String,
    vote: Number
})

let QuestionSchema = new mongoose.Schema({
    headline: String,
    question: String,
    comments: [AnswerSchema]
});

let Question = mongoose.model('Question', QuestionSchema);
let Answer = mongoose.model('Answer', AnswerSchema);



/****** Routes *****/
app.use('/api/questions/', (req, res) => {
    Question.find({}, (err, questions) => {
        res.json(questions)
    });
});
app.use('/api/question/:id', (req, res) => {
    Question.findOne({ _id: req.params.id }, (err, question) => {
        res.json(question)
    })
});

app.post('/api/question/', (req, res) => {
    let newPost = new Question(req.body);
    newPost.save();
    res.json({ msg: `You new book is posted` });
});

app.put('/api/comment/:id', (req, res) => {

    Question.findOne({ _id: req.params.id }, (err, question) => {
        let commentId = question.comments.length;
        question.comments.push({
            comment: req.body.comment,
            vote: 0,
            id: commentId
        })
        question.save();
    })
})

app.put('/api/upvote/:id/:commentId', (req, res) => {
    Question.findOne({ _id: req.params.id }).exec(function (err, question) {
        question.comments.find((elem) => elem._id == req.params.commentId).vote = req.body.vote;
        question.save();
    })
});
app.put('/api/downvote/:id/:commentId', (req, res) => {
    Question.findOne({ _id: req.params.id }).exec(function (err, question) {
        question.comments.find((elem) => elem._id == req.params.commentId).vote = req.body.vote;
        question.save();
    })
});


/****** Error handling ******/

app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send({ msg: 'Something broke!' })
});

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'));
});


app.listen(port, () => console.log(`API running on port ${port}!`));

