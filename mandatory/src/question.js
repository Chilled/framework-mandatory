import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Answers from './answer';
import AnwserQuestion from './addComment';

class question extends Component {
    api_url = process.env.REACT_APP_API_URL;
    constructor(props) {
        super(props);
        this.state = {
            question: [],
        }
    }
    componentDidMount() {
        this.getQuestion();
    }

    getQuestion() {
        fetch(`${this.api_url}/question/${this.props.match.params.id}`)
            .then(response => response.json())
            .then(json => {
                this.setState({
                    question: json
                })
            })
    }
    render() {
        let answerList = []
        if (this.state.question.comments)
            this.state.question.comments.forEach((elem, i) => {
                answerList.push(
                    <Answers answer={elem} questionId={this.state.question._id} />
                );
            })
        return (
            <div>
                <h1 style={{ width: 100 + '%' }}>
                    {this.state.question.headline}
                </h1>
                <div className={'question_wrapper'}>
                    <p className={'question_paragraf'}>
                        {this.state.question.question}
                    </p>
                </div>
                <hr />
                <AnwserQuestion id={this.state.question._id} />
                <ul className={'commentList'}>
                    {answerList}
                </ul>
            </div>
        )
    }
}

export default question