import React, { Component } from 'react';

class anwsers extends Component {
    api_url = process.env.REACT_APP_API_URL;

    constructor(props) {
        super(props)
        this.upvote = this.upvote.bind(this)
        this.downvote = this.downvote.bind(this)
    }

    upvote(e) {
        e.preventDefault();
        console.log(e.target);
        fetch(`${this.api_url}/upvote/${this.props.questionId}/${e.target.className}`, {
            method: 'PUT',
            body: JSON.stringify(
                {
                    vote: this.props.answer.vote + 1
                }
            ),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => console.log('Success:', JSON.stringify(response)))
            .catch(error => console.error('Error:', error));
    }
    downvote(e) {
        e.preventDefault();
        console.log(e.target);
        fetch(`${this.api_url}/downvote/${this.props.questionId}/${e.target.className}`, {
            method: 'PUT',
            body: JSON.stringify(
                {
                    vote: this.props.answer.vote - 1
                }
            ),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => console.log('Success:', JSON.stringify(response)))
            .catch(error => console.error('Error:', error));
    }

    render() {
        return (
            <li>
                <div>
                    <button className={this.props.answer._id} onClick={this.upvote}>upvote</button>
                    <br />
                    {this.props.answer.vote}
                    <br />
                    <button className={this.props.answer._id} onClick={this.downvote}>downvote</button>
                </div>
                <p>
                    {this.props.answer.comment}
                </p>
            </li>
        )
    }
}

export default anwsers