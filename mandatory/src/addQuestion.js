import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";

class addQuestion extends Component {
  api_url = process.env.REACT_APP_API_URL;

    constructor(props) {
        super(props);
        this.state = {
            api_url: props.api_url,
            headline: String,
            question: String,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChangeHeadline = this.onChangeHeadline.bind(this)
        this.onChangeQuestion = this.onChangeQuestion.bind(this)
    }

    onChangeHeadline(e) {
        let value = e.target.value;
        console.log('headline: ' + value)
        this.setState({
            headline: value
        })
    }
    onChangeQuestion(e) {
        let value = e.target.value;
        console.log('question: ' + value)
        this.setState({
            question: value
        })
    }


    handleSubmit(e) {
        e.preventDefault();
        fetch(`${this.api_url}/question`, {
            method: 'POST',
            body: JSON.stringify(
                {
                    headline: this.state.headline,
                    question: this.state.question,
                    comments: []
                }
            ),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => console.log('Success:', JSON.stringify(response)))
            .catch(error => console.error('Error:', error));
    }

    render() {
        return (           
            <form>
                <input type="text" placeholder="Title" className={'headline'} onChange={this.onChangeHeadline} />
                <br />
                <textarea placeholder="question..." id="question" onChange={this.onChangeQuestion} />
                <br />
                <button onClick={this.handleSubmit}>Send</button>
            </form>
        )
    }
}

export default addQuestion;