import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";

import './App.scss';
import Questions from './questions';
import AddQuestion from './addQuestion';
import Question from './question';
import Menu from './menu';

class App extends Component {
  api_url = process.env.REACT_APP_API_URL;

  constructor(props) {
    super(props);

    this.state = {
      questions: []
    }

  }

  componentDidMount() {
    this.getQuestions();
  }
  getById(id) {
    return this.state.questions.find((elem) => elem._id === Number(id));
  }
  getQuestions() {
    fetch(`${this.api_url}/questions`)
      .then(response => response.json())
      .then(json => {
        console.log(json)
        this.setState({
          questions: json
        })
      })

  }

  render() {
    return (
      <Router>
        <div className="grid-container">
          <nav className="grid-header">
            <div className="center">
              <Menu />
            </div>
          </nav>
          <section className="grid-content">
            <div className={'center'}>
              <Switch>
                <Route exact path={'/'}
                  render={(props) => <Questions {...props} questions={this.state.questions} />}
                />

                <Route exact path={'/question/:id'} render={(props) => <Question {...props} question={this.getById(props.match.params.id)} />} />

                <Route exact path={'/ask'}
                  render={(props) => <AddQuestion {...props} api_url={this.api_url} />} />
              </Switch>
            </div>
          </section>
        </div>
      </Router>
    );
  }
}

export default App;
