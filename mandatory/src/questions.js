import React, { Component } from 'react';
import { Link } from "react-router-dom";
// import Link from "react-router-dom/es/Link";

class questions extends Component {
    render() {
        let list = []
        this.props.questions.forEach((elem) => {
            let comments = (elem.upvote + elem.downvote);
            list.push(
                <Link key={elem.id} to={'/question/' + elem._id}  >
                    <div className="card">
                        <ul>
                            <li key={elem.id}>
                                <span>{comments}</span>
                                <span>Votes</span>
                            </li>
                            <li key={elem.id + 1}>
                                <span>{elem.comments.length}</span>
                                <span>comments</span>
                            </li>
                        </ul>
                        <div className="card-content">
                            <span>
                                {elem.headline}
                            </span>
                            <span>John doe</span>
                        </div>
                    </div>
                </Link>
            )
        })
        return (
            <div>
                {list}
            </div>
        )
    }
}

export default questions;