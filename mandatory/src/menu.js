
import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";

class Menu extends Component {

    render() {
        return (
            <ul>
                <li key={'home'}>
                    <Link to={'/'}>Home</Link>
                </li>
                <li key={'ask'}>
                    <Link to={'/ask'}>Ask</Link>
                </li>
            </ul>
        )
    }
}

export default Menu