import React, { Component } from 'react';

class addComment extends Component {
    api_url = process.env.REACT_APP_API_URL;

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.state = {
            anwser: "",
        }
    }
    onChange(e) {
        let value = e.target.value;
        this.setState({
            anwser: value
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        fetch(`${this.api_url}/comment/${this.props.id}`, {
            method: 'PUT',
            body: JSON.stringify(
                {
                    comment: this.state.anwser,
                    vote: 0,
                    questionId: this.props.id
                }
            ),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
            .then(response => console.log('Success:', JSON.stringify(response)))
            .catch(error => console.error('Error:', error));
    }



    render() {
        return (
            <div>
                <form>
                    <textarea value={this.state.commentText} onChange={this.onChange} placeholder={'Type your anwser'} />
                    <br />
                    <button onClick={this.handleSubmit}>Answser</button>
                </form>
            </div>
        )
    }
}

export default addComment